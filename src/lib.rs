//! # Maco
//!
//! Maco is a simple Mattermost API Connector in Rust. It is being written
//! for my upcoming GTK3 Mattermost client, but it is intended to be flexible
//! enough to be reused. Feel free to use it, extend it, or whatever!
//!
//! Let me know if/how you use it, I'd love to see what people come up with!
//!
//! For examples, see the `examples` directory
//!
//! # Contributing
//!
//! Anyone is welcome to contribute. Please read the [guidelines for contributing](
//! https://gitlab.com/pop-planet/maco/blob/master/CONTRIBUTING.md).

extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate reqwest;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

mod auth;
mod errors;
mod endpoints;

pub use auth::*;
pub use errors::*;
pub use endpoints::*;

use reqwest::{header::*, Client};

pub type MacoResult<T> = Result<T, Error>;

/// Type for constructing a new login request.
#[derive(Debug)]
pub struct Maco<'a> {
    /// The URI of the server that is to be connected to.
    base_uri: &'a str,
    /// The connection pool for making HTTP/S requests.
    client: Client,
}

impl<'a> AuthGet for Maco<'a> {
    fn base_uri(&self) -> &str { self.base_uri }
    fn client(&self) -> &Client { &self.client }
}

impl<'a> Maco<'a> {
    /// Construct a new login request, defining the server to connect to.
    pub fn new(base_uri: &'a str) -> Self {
        Self {
            base_uri: match base_uri.rfind(|b| b != '/') {
                Some(pos) => &base_uri[..=pos],
                None => base_uri
            },
            client: Client::new()
        }
    }

    /// Attempt to login to the mattermost server.
    ///
    /// An `Authentication` provides the different means of possible login methods. The
    /// most basic are logging in via an ID and password to receive a new auth token, or
    /// by logging in with an existing auth token. Either way, the credentials will be
    /// verified, and a `Connected` will be returned on success.
    ///
    /// # Notes
    /// On failure, `Self` is returned with the error so that a login may be attempted again.
    pub fn login(self, auth: AuthToken) -> Result<Connected<'a>, (Self, Error)> {
        // Attempt to authenticate with the given credentials.
        let authenticate = match auth {
            AuthToken::Create(auth) => {
                self.get_token(auth)
            },
            AuthToken::Use(token) => {
                self.get_user(&token)
                    .map(|user| (token, user))
            }
        };

        match authenticate {
            Ok((auth_token, user)) => {
                let connected = Connected {
                        client: self.client,
                        base_uri: self.base_uri,
                        auth_token,
                        auth_type: "BEARER",
                        user
                    };
                Ok(connected)
            }
            Err(why) => Err((self, why))
        }
    }

    /// When authenticating via a token, validate it by getting user info.
    fn get_user(&self, auth_token: &str) -> MacoResult<User> {
        self.get_with_auth(&["BEARER ", auth_token].concat(), "/users/me")
            .and_then(|text| serde_json::from_str(&text).map_err(Into::into))
    }

    /// Get an authentication token via a login and password.
    pub fn get_token(&self, auth: IDAuth<'a>) -> MacoResult<(String, User)> {
        let (login_key, login_value) = match auth.login {
            Login::Id(login) => ("id", login),
            Login::LoginId(login) => ("login_id", login)
        };

        let message = json!({
            "device_id": auth.device_id.unwrap_or(""),
            login_key: login_value,
            "password": auth.password,
            "token": auth.token.unwrap_or("")
        }).to_string();

        let mut response = self.client.post(&get_uri_for_method(self.base_uri, "/users/login"))
            .header(CONTENT_TYPE, "application/json")
            .body(message)
            .send()?;

        let status = response.status();
        if ! status.is_success() {
            return Err(Error::HTTP { status })
        }

        let auth_token = response.headers().get("token")
            .ok_or_else(|| Error::TokenNotFound)
            .and_then(|t|
                String::from_utf8(t.as_bytes().to_owned())
                    .map_err(|_| Error::TokenInvalidUtf8)
            )?;

        let user = response.text()
            .map_err::<errors::Error, _>(Into::into)
            .and_then(|text|
                serde_json::from_str(&text)
                .map_err(Into::into)
            )?;

        Ok((auth_token, user))
    }
}

/// An authenticated Maco session.
#[derive(Debug)]
pub struct Connected<'a> {
    base_uri: &'a str,
    client: Client,
    auth_token: String,
    auth_type: &'static str,
    user: User,
}

impl<'a> AuthGet for Connected<'a> {
    fn base_uri(&self) -> &str { self.base_uri }
    fn client(&self) -> &Client { &self.client }
}

impl<'a> Connected<'a> {
    /// This is required for the authorization header when making requests.
    fn auth_string(&self) -> String {
        [self.auth_type, " ", &self.auth_token].concat()
    }

    pub fn get_my_teams(&self) -> MacoResult<Teams> {
        let auth = self.auth_string();
        self.get_with_auth(&auth, "/users/me/teams")
            .and_then(|teams| serde_json::from_str(&teams).map_err(Into::into))
    }

    pub fn get_my_channels(&self) -> MacoResult<Vec<Channels>> {
        let auth = self.auth_string();

        let teams = self.get_my_teams()?;
        let mut channels_list = Vec::new();

        for team in &teams {
            let channels = self.get_with_auth(&auth, &["/users/me/teams/", &team.id, "/channels"].concat())?;
            let channels: Channels = serde_json::from_str(&channels).map_err::<serde_json::Error, _>(Into::into)?;
            channels_list.push(channels);
        }
        Ok(channels_list)
    }

    pub fn get_my_team_members(&self) -> MacoResult<TeamMembers> {
        let auth = self.auth_string();
        let members = self.get_with_auth(&auth, "/users/me/teams/members")?;
        serde_json::from_str(&members).map_err(Into::into)
    }

    pub fn get_my_team_unreads(&self) -> MacoResult<TeamUnread> {
        let auth = self.auth_string();
        let members = self.get_with_auth(&auth, "/users/me/teams/unread")?;
        serde_json::from_str(&members).map_err(Into::into)
    }
}

/// Given the base URI for the server and an API method, construct the
/// API endpoint for a specific call.
fn get_uri_for_method(base: &str, method: &str) -> String {
    [base, "/api/v4", method].concat()
}
