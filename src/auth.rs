use super::get_uri_for_method;
use errors::Error;
use reqwest::{Client, header::{AUTHORIZATION, CONTENT_TYPE}};

pub(crate) trait AuthGet {
    fn base_uri(&self) -> &str;

    fn client(&self) -> &Client;

    /// Send a GET request to the server for the `path`, with the given `auth` header.
    fn get_with_auth(&self, auth: &str, path: &str) -> Result<String, Error> {
        let uri = get_uri_for_method(self.base_uri(), path);
        let mut response = self.client().get(&uri)
            .header(AUTHORIZATION, auth)
            .header(CONTENT_TYPE, "application/json")
            .send()?;

        let status = response.status();
        if ! status.is_success() {
            return Err(Error::HTTP { status })
        }

        let text = response.text()?;
        Ok(text)
    }
}

/// Specifies whether to create a new token, or use an existing one.
pub enum AuthToken<'a> {
    Create(IDAuth<'a>),
    Use(String),
}

impl<'a> AuthToken<'a> {
    pub fn create(user: &'a str, pass: &'a str) -> Self {
        AuthToken::Create(IDAuth::new(Login::LoginId(user), pass))
    }
}

/// Specifies whether to login via `id` or `login_id`.
pub enum Login<'a> {
    Id(&'a str),
    LoginId(&'a str),
}

/// Credentials for authenticating to the server and requesting a new `auth_token`.
pub struct IDAuth<'a> {
    pub login: Login<'a>,
    pub password: &'a str,
    pub token: Option<&'a str>,
    pub device_id: Option<&'a str>,
    pub ldap_only: bool,
}

impl<'a> IDAuth<'a> {
    pub fn new(login: Login<'a>, password: &'a str) -> Self {
        Self {
            login,
            password,
            token: None,
            device_id: None,
            ldap_only: true
        }
    }

    pub fn token(mut self, token: &'a str) -> Self {
        self.token = Some(token);
        self
    }

    pub fn device_id(mut self, device_id: &'a str) -> Self {
        self.device_id = Some(device_id);
        self
    }

    pub fn ldap_only(mut self, only: bool) -> Self {
        self.ldap_only = only;
        self
    }
}
