mod team_member;
mod unread;

pub use self::team_member::{TeamMember, TeamMembers};
pub use self::unread::{TeamUnread};

#[derive(Debug, Deserialize)]
pub struct Team {
    pub id: String,
    pub create_at: i64,
    pub update_at: i64,
    pub delete_at: i64,
    pub display_name: String,
    pub name: String,
    pub description: String,
    pub email: String,
    pub team_type: String,
    pub company_name: String,
    pub allowed_domains: String,
    pub invite_id: String,
    pub allow_open_invite: bool,
    pub last_team_icon_update: Option<i64>,
    pub scheme_id: String,
}

#[derive(Debug, Deserialize)]
pub struct Teams(Vec<Team>);

impl AsRef<[Team]> for Teams {
    fn as_ref(&self) -> &[Team] {
        &self.0
    }
}

impl AsMut<[Team]> for Teams {
    fn as_mut(&mut self) -> &mut [Team] {
        &mut self.0
    }
}

impl IntoIterator for Teams {
    type Item = Team;
    type IntoIter = ::std::vec::IntoIter<Team>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<'a> IntoIterator for &'a Teams {
    type Item = &'a Team;
    type IntoIter = ::std::slice::Iter<'a, Team>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a> IntoIterator for &'a mut Teams {
    type Item = &'a mut Team;
    type IntoIter = ::std::slice::IterMut<'a, Team>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}
