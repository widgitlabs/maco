#[derive(Debug, Deserialize)]
pub struct TeamMember {
    pub team_id: String,
    pub user_id: String,
    pub roles: String,
    pub delete_at: i64,
    pub scheme_user: bool,
    pub scheme_admin: bool,
    pub explicit_roles: String,
}

#[derive(Debug, Deserialize)]
pub struct TeamMembers(pub Vec<TeamMember>);
