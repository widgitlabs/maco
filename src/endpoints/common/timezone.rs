#[derive(Debug, Deserialize)]
pub struct Timezone {
    #[serde(rename = "automaticTimezone")]
    pub automatic_timezone: String,
    #[serde(rename = "manualTimezone")]
    pub manual_timezone: String,
    #[serde(rename = "useAutomaticTimezone")]
    pub use_automatic_timezone: String,
}
