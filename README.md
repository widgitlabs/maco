# Welcome to our GitLab Repository

**Maco** is a simple **M**attermost **A**PI **CO**nnector in Rust. While originally envisioned as a personal pet project, Maco has become a collaborative project under the [Pop!_Planet](https://pop-planet.info) name.

Maco is being written for our Mattermost client [Silva](https://gitlab.com/pop-planet/silva), but it is intended to be flexible enough to be reused. Feel free to use it in your own project, extend it, or whatever!

Let us know if/how you use it, we'd love to see what people come up with!

## Dependencies

* cargo (Rust 1.30.0)

## Bugs

If you find an issue, please let us know [here](https://gitlab.com/pop-planet/maco/issues)!

## Contributions

Anyone is welcome to contribute. Please read the [guidelines for contributing](https://gitlab.com/pop-planet/maco/blob/master/CONTRIBUTING.md).