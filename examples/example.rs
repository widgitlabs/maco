extern crate clap;
extern crate maco;
extern crate promptly;

use clap::{App, AppSettings, Arg, SubCommand};
use maco::{Maco, AuthToken};
use promptly::prompt;

/// Examples herein are far from final, and are mostly here just for my
/// reference during the development process. That said, they should
/// roughly represent the basic idea of what we're going for.
fn main() {
    let matches = App::new("mount")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(SubCommand::with_name("credentials"))
        .subcommand(SubCommand::with_name("token")
            .arg(Arg::with_name("token").required(true)))
        .get_matches();

    let (mut user, mut pass): (String, String);
    let mut connector = Maco::new("https://chat.pop-os.org/");

    let connection = match matches.subcommand() {
        ("credentials", _) => {
            loop {
                user = prompt("username");
                pass = prompt("password");
                let auth = AuthToken::create(&user, &pass);
                match connector.login(auth) {
                    Ok(connection) => {
                        break connection;
                    },
                    Err((conn, why)) => {
                        eprintln!("failed to login: {}", why);
                        connector = conn;
                    }
                }
            }
        }
        ("token", Some(matches)) => {
            let token = matches.value_of("token").unwrap().to_owned();
            connector.login(AuthToken::Use(token)).unwrap()
        }
        _ => unreachable!()
    };

    eprintln!("{:?}", connection);
}
